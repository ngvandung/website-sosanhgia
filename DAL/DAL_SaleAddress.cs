﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_SaleAddress : Connecting
    {
        public DataTable getSaleAddress()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getSaleAddress", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable checkSaleAddress(string saleAddressName)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select * from diemban where tendb = N'" + saleAddressName + "'", getConn()); //Tên store procedure
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable da = new DataTable();
                adapter.Fill(da);
                closeConnect(); //Đóng kết nối
                return da;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getSaleAddressFromProductId(int productId)
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getSaleAddress", getConn());
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@masp", productId);
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addSaleAddress(DTO_SaleAddress dto_saleaddress)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addSaleAddress", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madb", dto_saleaddress.SaleAddressId);
                cmd.Parameters.AddWithValue("@masp", dto_saleaddress.ProductId);
                cmd.Parameters.AddWithValue("@duongdan", dto_saleaddress.Link);
                cmd.Parameters.AddWithValue("@tendb", dto_saleaddress.SaleAddressName);
                cmd.Parameters.AddWithValue("@anhdiemban", dto_saleaddress.SaleAddressImage);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteSaleAddress(int saleAddressId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteSaleAddress", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madb", saleAddressId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editSaleAddress(DTO_SaleAddress dto_saleaddress)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editSaleAddress", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madb", dto_saleaddress.SaleAddressId);
                cmd.Parameters.AddWithValue("@masp", dto_saleaddress.ProductId);
                cmd.Parameters.AddWithValue("@duongdan", dto_saleaddress.Link);
                cmd.Parameters.AddWithValue("@tendb", dto_saleaddress.SaleAddressName);
                cmd.Parameters.AddWithValue("@anhdiemban", dto_saleaddress.SaleAddressImage);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementSaleAddress", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }

        public int getSaleAddressIdFromSaleAddressName(string saleAddressName)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select madb from diemban where tendb = '" + saleAddressName + "'", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp;
            }
            catch
            {
                closeConnect();
                return 0;
            }
        }
    }
}
