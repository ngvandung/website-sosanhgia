﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_Voting : Connecting
    {
        public DataTable getVoting()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getVoting", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addVoting(DTO_Voting dto_voting)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addVoting", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matk", dto_voting.UserId);
                cmd.Parameters.AddWithValue("@masp", dto_voting.ProductId);
                cmd.Parameters.AddWithValue("@danhgia", dto_voting.Vote);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteVoting(int UserId, int ProductId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteVoting", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matk", UserId);
                cmd.Parameters.AddWithValue("@masp", ProductId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editVoting(DTO_Voting dto_voting)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editVoting", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matk", dto_voting.UserId);
                cmd.Parameters.AddWithValue("@masp", dto_voting.ProductId);
                cmd.Parameters.AddWithValue("@danhgia", dto_voting.Vote);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementVoting", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
