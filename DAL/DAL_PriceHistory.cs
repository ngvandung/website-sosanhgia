﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class DAL_PriceHistory : Connecting
    {
        public DataTable getPriceHistory()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getPriceHistory", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable checkPriceHistory(int productId, int saleAddressId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select * from lichsugia where masp = '" + productId + "' and madb = '" + saleAddressId + "'", getConn()); //Tên store procedure
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable da = new DataTable();
                adapter.Fill(da);
                closeConnect(); //Đóng kết nối
                return da;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getPriceHistory_Product_SaleAdress(int saleAddressId, int productId)
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getPriceHistory_Product_SaleAdress", getConn());
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@madb", saleAddressId);
                command.Parameters.AddWithValue("@masp", productId);
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addPriceHistory(DTO_PriceHistory dto_pricehistory)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addPriceHistory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madb", dto_pricehistory.SaleAddressId);
                cmd.Parameters.AddWithValue("@masp", dto_pricehistory.ProductId);
                cmd.Parameters.AddWithValue("@ngay", dto_pricehistory.Date);
                cmd.Parameters.AddWithValue("@gia", dto_pricehistory.Price);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deletePriceHistory(int saleAddressId, int productId, string date)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deletePriceHistory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madb", saleAddressId);
                cmd.Parameters.AddWithValue("@masp", productId);
                cmd.Parameters.AddWithValue("@ngay", date);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editPriceHistory(DTO_PriceHistory dto_pricehistory)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editPriceHistory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madb", dto_pricehistory.SaleAddressId);
                cmd.Parameters.AddWithValue("@masp", dto_pricehistory.ProductId);
                cmd.Parameters.AddWithValue("@ngay", dto_pricehistory.Date);
                cmd.Parameters.AddWithValue("@gia", dto_pricehistory.Price);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }
    }
}
