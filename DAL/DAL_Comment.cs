﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_Comment : Connecting
    {
        public DataTable getComment()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getComment", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getCommentFromProductId(int productId)
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getCommentfromProduct", getConn());
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@masp", productId);
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addComment(DTO_Comment dto_Comment)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addComment", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matk", dto_Comment.UserId);
                cmd.Parameters.AddWithValue("@masp", dto_Comment.ProductId);
                cmd.Parameters.AddWithValue("@noidung", dto_Comment.Content);
                cmd.Parameters.AddWithValue("@ngay", dto_Comment.Date);
                cmd.Parameters.AddWithValue("@mabl", dto_Comment.CommentId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteComment(int commentId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteComment", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@mabl", commentId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editComment(DTO_Comment dto_Comment)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editComment", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@noidung", dto_Comment.Content);
                cmd.Parameters.AddWithValue("@ngay", dto_Comment.Date);
                cmd.Parameters.AddWithValue("@mabl", dto_Comment.CommentId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementComment", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
