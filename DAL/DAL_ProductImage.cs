﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_ProductImage : Connecting
    {
        public DataTable getProductImage()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getProductImage", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable checkProductImage(string linkImage)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select * from anhsp where anh = '" + linkImage + "'", getConn()); //Tên store procedure
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable da = new DataTable();
                adapter.Fill(da);
                closeConnect(); //Đóng kết nối
                return da;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getProductImageFromProductId(int productId)
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getProductImageFromProductId", getConn());
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@masp", productId);
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addProductImage(DTO_ProductImage dto_productimage)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addProductImage", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@maanh", dto_productimage.ImageId);
                cmd.Parameters.AddWithValue("@masp", dto_productimage.ProductId);
                cmd.Parameters.AddWithValue("@anh", dto_productimage.Image);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteProductImage(int ImageId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteProductImage", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@maanh", ImageId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editProductImage(DTO_ProductImage dto_productimage)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editProductImage", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@maanh", dto_productimage.ImageId);
                cmd.Parameters.AddWithValue("@masp", dto_productimage.ProductId);
                cmd.Parameters.AddWithValue("@anh", dto_productimage.Image);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment(int productId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementProductImage", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@masp", productId);
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
