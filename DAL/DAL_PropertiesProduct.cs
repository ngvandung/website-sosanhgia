﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class DAL_PropertiesProduct : Connecting
    {
        public DataTable getPropertiesProduct()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getPropertiesProduct", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getPropertiesProductfromProductId(int productId)
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getPropertiesProductfromProductId", getConn());
                command.Parameters.AddWithValue("@masp", productId);
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addPropertiesProduct(DTO_PropertiesProduct dto_propertiesproduct)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addPropertiesProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matt", dto_propertiesproduct.PropertiesId);
                cmd.Parameters.AddWithValue("@masp", dto_propertiesproduct.ProductId);
                cmd.Parameters.AddWithValue("@value", dto_propertiesproduct.Value);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deletePropertiesProduct(int propertiesProductId, int productId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deletePropertiesProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matt", propertiesProductId);
                cmd.Parameters.AddWithValue("@masp", productId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editPropertiesProduct(DTO_PropertiesProduct dto_propertiesproduct)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editPropertiesProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matt", dto_propertiesproduct.PropertiesId);
                cmd.Parameters.AddWithValue("@masp", dto_propertiesproduct.ProductId);
                cmd.Parameters.AddWithValue("@value", dto_propertiesproduct.Value);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }
    }
}
