﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class DAL_Properties : Connecting
    {
        public DataTable getProperties()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getProperties", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addProperties(DTO_Properties dto_properties)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addProperties", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matt", dto_properties.PropertiesId);
                cmd.Parameters.AddWithValue("@tentt", dto_properties.PropertiesName);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteProperties(int PropertiesId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteProperties", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matt", PropertiesId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editProperties(DTO_Properties dto_properties)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editProperties", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@matt", dto_properties.PropertiesId);
                cmd.Parameters.AddWithValue("@tentt", dto_properties.PropertiesName);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementProperties", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
