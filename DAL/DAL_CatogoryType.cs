﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class DAL_CatogoryType : Connecting
    {
        public DataTable getCatogoryType()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getCatogoryType", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }


        public DataTable checkCatogoryType(string typeName)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select * from loaidanhmuc where tenloai = N'" + typeName + "'", getConn()); //Tên store procedure
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable da = new DataTable();
                adapter.Fill(da);
                closeConnect(); //Đóng kết nối
                return da;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public int getCatogoryTypefromTypeName(string typeName)
        {
            try
            {
                openConnect();
                SqlCommand command = new SqlCommand("select maloai from loaidanhmuc where tenloai = '" + typeName + "'", getConn());
                int temp = Convert.ToInt32(command.ExecuteScalar());
                closeConnect();
                return temp;
            }
            catch
            {
                closeConnect();
                return 0;
            }
        }

        public bool addCatogoryType(DTO_CatogoryType dto_catogorytype)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addCatogoryType", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@maloai", dto_catogorytype.TypeId);
                cmd.Parameters.AddWithValue("@tenloai", dto_catogorytype.TypeName);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteCatogoryType(int typeId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteCatogoryType", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@maloai", typeId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editCatogoryType(DTO_CatogoryType dto_catogorytype)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editCatogoryType", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@maloai", dto_catogorytype.TypeId);
                cmd.Parameters.AddWithValue("@tenloai", dto_catogorytype.TypeName);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementCatogoryType", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
