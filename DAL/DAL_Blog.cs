﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_Blog : Connecting
    {
        public DataTable getBlog()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getBlog", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getBlogFromUserId(int userId)
        {
            try
            {
                SqlCommand command = new SqlCommand("select * from blog where matk = '" + userId + "'", getConn());
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public bool addBlog(DTO_Blog dto_blog)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addBlog", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@mabv", dto_blog.BlogId);
                cmd.Parameters.AddWithValue("@noidung", dto_blog.Content);
                cmd.Parameters.AddWithValue("@matk", dto_blog.UserId);
                cmd.Parameters.AddWithValue("@anhdaidien", dto_blog.Image);
                cmd.Parameters.AddWithValue("@ngay", dto_blog.Date);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteBlog(int blogId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteBlog", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@mabv", blogId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editBlog(DTO_Blog dto_blog)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editBlog", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@mabv", dto_blog.BlogId);
                cmd.Parameters.AddWithValue("@noidung", dto_blog.Content);
                cmd.Parameters.AddWithValue("@matk", dto_blog.UserId);
                cmd.Parameters.AddWithValue("@anhdaidien", dto_blog.Image);
                cmd.Parameters.AddWithValue("@ngay", dto_blog.Date);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementBlog", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
