﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Catogory : Connecting
    {
        public DataTable getCatogory()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getCatogory", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getCatogoryByCatogoryTypeId(int typeId)
        {
            try
            {
                SqlCommand command = new SqlCommand("select loaidanhmuc.maloai, madm, tendm from danhmuc, loaidanhmuc where loaidanhmuc.maloai = danhmuc.maloai and loaidanhmuc.maloai = '" + typeId + "'", getConn());
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable checkCatogory(string catogoryName)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select * from loaidanhmuc where tenloai = '" + catogoryName + "'", getConn()); //Tên store procedure
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable da = new DataTable();
                adapter.Fill(da);
                closeConnect(); //Đóng kết nối
                return da;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }
        public bool addCatogory(DTO_Catogory dto_catogory)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addCatogory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madm", dto_catogory.CatogoryId);
                cmd.Parameters.AddWithValue("@tendm", dto_catogory.CatogoryName);
                cmd.Parameters.AddWithValue("@maloai", dto_catogory.TypeId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteCatogory(int catogoryId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteCatogory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madm", catogoryId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editCatogory(DTO_Catogory dto_catogory)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editCatogory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@madm", dto_catogory.CatogoryId);
                cmd.Parameters.AddWithValue("@tendm", dto_catogory.CatogoryName);
                cmd.Parameters.AddWithValue("@maloai", dto_catogory.TypeId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int getCatogoryId(string catogoryName)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select madm from danhmuc where tendm ='" + catogoryName + "'", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementCatogory", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch
            {
                closeConnect();
                return 1;
            }
        }
    }
}
