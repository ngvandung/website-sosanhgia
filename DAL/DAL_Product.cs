﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Product : Connecting
    {
        public DataTable getAllProducts()
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getAllProducts", getConn());
                command.CommandType = CommandType.StoredProcedure;
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }


        public DataTable getTop4Product(int catogoryId)
        {
            try
            {
                SqlCommand command = new SqlCommand("select top(4) sanpham.masp, tensp, gia from sanpham, lichsugia, danhmuc where lichsugia.masp = sanpham.masp and danhmuc.madm = sanpham.madm and madm = '" + catogoryId + "' order by gia", getConn());
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getProductsByProductId(int productId)
        {
            try
            {
                SqlCommand command = new SqlCommand("select * from sanpham where masp = '" + productId + "'", getConn());
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable checkProduct(string productName)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("select * from sanpham where tensp = N'" + productName + "'", getConn()); //Tên store procedure
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable da = new DataTable();
                adapter.Fill(da);
                closeConnect(); //Đóng kết nối
                return da;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public DataTable getByDanhMuc(int categoryId)
        {
            try
            {
                SqlCommand command = new SqlCommand("stp_getByDanhMuc", getConn());
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@danhmuc", categoryId);
                openConnect();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                closeConnect();
                return dt;
            }
            catch
            {
                closeConnect();
                return null;
            }
        }

        public int getProductIdFromProductName(string productName)
        {
            try
            {
                SqlCommand command = new SqlCommand("select masp from sanpham where tensp = '" + productName + "'", getConn());
                openConnect();
                int temp = Convert.ToInt32(command.ExecuteScalar());
                closeConnect();
                return temp;
            }
            catch
            {
                closeConnect();
                return 0;
            }
        }

        public bool addProduct(DTO_Product dto_product)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_addProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@masp", dto_product.ProductId);
                cmd.Parameters.AddWithValue("@tensp", dto_product.NameProduct);
                cmd.Parameters.AddWithValue("@thongtin", dto_product.InfoProduct);
                cmd.Parameters.AddWithValue("@madm", dto_product.CategoryId);
                cmd.Parameters.AddWithValue("@math", dto_product.BrandId);
                cmd.Parameters.AddWithValue("@diemdanhgia", dto_product.Point);
                cmd.Parameters.AddWithValue("@matk", dto_product.UserId);
                cmd.Parameters.AddWithValue("@anhdaidien", dto_product.Avt);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch(Exception e)
            {
                closeConnect();
                return false;
            }
        }

        public bool deleteProduct(int productId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_deleteProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@masp", productId);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public bool editProduct(DTO_Product dto_product)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_editProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@masp", dto_product.ProductId);
                cmd.Parameters.AddWithValue("@tensp", dto_product.NameProduct);
                cmd.Parameters.AddWithValue("@thongtin", dto_product.InfoProduct);
                cmd.Parameters.AddWithValue("@madm", dto_product.CategoryId);
                cmd.Parameters.AddWithValue("@math", dto_product.BrandId);
                cmd.Parameters.AddWithValue("@diemdanhgia", dto_product.Point);
                cmd.Parameters.AddWithValue("@matk", dto_product.UserId);
                cmd.Parameters.AddWithValue("@avt", dto_product.Avt);
                cmd.ExecuteNonQuery();
                closeConnect(); //Đóng kết nối
                return true;
            }
            catch
            {
                closeConnect();
                return false;
            }
        }

        public int increment()
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_incrementProduct", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp + 1;
            }
            catch(Exception e)
            {
                closeConnect();
                return 1;
            }
        }

        public int getPoint(int productId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_getPoint", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@masp", productId);
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp;
            }
            catch
            {
                closeConnect();
                return 0;
            }
        }

        public int getUserIdfromProductId(int productId)
        {
            try
            {
                openConnect();
                SqlCommand cmd = new SqlCommand("stp_getUserIdfromProductId", getConn()); //Tên store procedure
                cmd.CommandType = CommandType.StoredProcedure; //Cho biết đây là store procedure
                cmd.Parameters.AddWithValue("@masp", productId);
                int temp = Convert.ToInt32(cmd.ExecuteScalar());
                closeConnect();
                return temp;
            }
            catch
            {
                closeConnect();
                return 0;
            }
        }
    }
}
