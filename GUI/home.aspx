﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="GUI.home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/public/css/home.css" rel="stylesheet" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
    <div class="container">
        <div class="lz-big-banners">
            <div class="banner">
                <a href="/r/redirect.php?ads_id=2346&amp;asub=direct~SSG" target="_blank" rel="nofollow">
                    <img src="https://img.sosanhgia.com/images/327x0/be332a47b7584748a7b128f76eb1ea28/row1col1.jpeg">
                </a>
            </div>
            <div class="banner">
                <a href="/r/redirect.php?ads_id=2347&amp;asub=direct~SSG" target="_blank" rel="nofollow">
                    <img src="https://img.sosanhgia.com/images/327x0/5ba9bf2cad1c41d29811fa4751edcacf/row1col2.jpeg">
                </a>
            </div>
            <div class="banner">
                <a href="/r/redirect.php?ads_id=2348&amp;asub=direct~SSG" target="_blank" rel="nofollow">
                    <img src="https://img.sosanhgia.com/images/327x0/a678493a69014408971b6acd47d6275f/row1col3.jpeg">
                </a>
            </div>                                                                                                                                                          </div>
    </div>
</div>    
    <div id="products" class="container-fluid text-center">
        <div class="row">
            <div class="container">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tivi">Tivi - Âm thanh</a></li>
                <li><a data-toggle="tab" href="#dienthoai">Điện thoại</a></li>
                <li><a data-toggle="tab" href="#maytinh">Máy tính</a></li>
                <li><a data-toggle="tab" href="#dogiadung">Đồ gia dụng</a></li>
              </ul>
              <div class="tab-content">
                <div id="tivi" class="tab-pane fade in active">
                  <ul>
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <li>
				                <div class="img-wrap">
				                    <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			                    </div>
			                    <div class="information" align="center">
				                    <div class="title">
				                        <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				                    </div>
				                    <div class="price">
				                        <span>16.900.000 đ</span>
				                        <br>
                                        <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				                    </div>
			                    </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                  </ul>
                </div>
                <div id="dienthoai" class="tab-pane fade">
                  <ul>
                    <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>
                            <li>
				                <div class="img-wrap">
				                    <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			                    </div>
			                    <div class="information" align="center">
				                    <div class="title">
				                        <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				                    </div>
				                    <div class="price">
				                        <span>16.900.000 đ</span>
				                        <br>
                                        <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				                    </div>
			                    </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                  </ul>
                </div>
                <div id="maytinh" class="tab-pane fade">
                  <ul>
                    <asp:Repeater ID="Repeater3" runat="server">
                        <ItemTemplate>
                            <li>
				                <div class="img-wrap">
				                    <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			                    </div>
			                    <div class="information" align="center">
				                    <div class="title">
				                        <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				                    </div>
				                    <div class="price">
				                        <span>16.900.000 đ</span>
				                        <br>
                                        <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				                    </div>
			                    </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                  </ul>
                </div>
                <div id="dogiadung" class="tab-pane fade">
                  <ul>
                    <asp:Repeater ID="Repeater4" runat="server">
                        <ItemTemplate>
                            <li>
				                <div class="img-wrap">
				                    <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			                    </div>
			                    <div class="information" align="center">
				                    <div class="title">
				                        <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				                    </div>
				                    <div class="price">
				                        <span>16.900.000 đ</span>
				                        <br>
                                        <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				                    </div>
			                    </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>     
        <!-- Container (Categorys Section) -->
    <div id="categorys" class="container-fluid text-center">
          <h2>Danh mục phổ biến</h2>
          <hr>
          <br>
          <div class="row slideanim">
            <div class="col-sm-4">
              <span class="glyphicon glyphicon-camera logo-small"></span>
              <h4>Máy ảnh</h4>
              <p></p>
            </div>
            <div class="col-sm-4">
              <span class="glyphicon glyphicon-phone logo-small"></span>
              <h4>Điện thoại</h4>
              <p></p>
            </div>
            <div class="col-sm-4">
              <i class="fas fa-laptop logo-small"></i>
              <h4>Máy tính</h4>
              <p></p>
            </div>
          </div>
          <div class="row slideanim">
            <div class="col-sm-4">
              <i class="fas fa-tv logo-small"></i>
              <h4>Ti vi</h4>
              <p></p>
            </div>
            <div class="col-sm-4">
              <i class="fas fa-charging-station logo-small"></i>
              <h4>Đồ gia dụng</h4>
              <p></p>
            </div>
            <div class="col-sm-4">
              <i class="fas fa-car logo-small"></i>
              <h4>Ô tô</h4>
              <p></p>
            </div>
          </div>    
        </div>
    <div id="quoangcao" class="container-fluid text-center">
            <h2>Khuyến mãi Hot nhất cho hôm nay</h2>
            <hr>
            <br>
            <div class="row">
              <div class="container">
                <div class="col-sm-4 qc-items">
                  <a href="" class="">
                    <img src="https://img.sosanhgia.com/images/494x0/9cdaec3b61d74de1825b3ccf6e6b8f2f/image.jpeg" class="img-qc">
                  </a>
                </div>
                <div class="col-sm-4 qc-items">
                  <a href="" class="">
                    <img src="https://img.sosanhgia.com/images/494x0/764cf84c363d436cb405ae1cc55d3acf/image.jpeg" class="img-qc">
                  </a>
                </div>
                <div class="col-sm-4 qc-items">
                  <a href="" class="">
                    <img src="https://img.sosanhgia.com/images/494x0/d2ec26282ea34341b6d97bce798a55e6/image.jpeg" class="img-qc">
                  </a>
                </div>
                <div class="col-sm-4 qc-items">
                  <a href="" class="">
                    <img src="https://img.sosanhgia.com/images/494x0/fc5b4bb3f89e4f0f8df0b9707b4e59c2/image.jpeg" class="img-qc">
                  </a>
                </div>
                <div class="col-sm-4 qc-items">
                  <a href="" class="">
                    <img src="https://img.sosanhgia.com/images/580x0/6ef9a4e885164c0c8b316ebaddeecdad/image.jpeg" class="img-qc">
                  </a>
                </div>
                <div class="col-sm-4 qc-items">
                  <a href="" class="">
                    <img src="https://img.sosanhgia.com/images/287x0/ddf88e90931f4f119dac90a0db2d40dd/image.jpeg" class="img-qc">
                  </a>
                </div> 
              </div>                 
            </div>
        </div>
    <div id="pots" class="container-fluid text-center">
          <h2>Thông tin mua sắm hữu ích</h2>
          <hr>
          <br>
          <div class="row">
            <div class="container">
                <asp:Repeater ID="rptpots" runat="server">
                    <ItemTemplate>
                        <div class="col-sm-6 qc-items">
                            <div class="pots-items">
                              <h4><%# Eval("tieude") %></h4>
                              <img src="<%# Eval("anh") %>">
                              <div class="short-description">
                                <p><%# Eval("noidung") %></p>
                                <a href="">Xem thêm ></a>
                              </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
          </div>     
        </div>
        <!-- Container (Contact Section) -->
    <div id="contact" class="container-fluid bg-grey">
          <h2 class="text-center">Liên hệ</h2>
          <div class="row">
            <div class="col-sm-5">
              <p>Liên lạc với chúng tôi và chúng tôi sẽ liên lạc lại với bạn trong vòng 24 giờ.</p>
              <p><span class="glyphicon glyphicon-map-marker"></span> Hà Nội, VN</p>
              <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
              <p><span class="glyphicon glyphicon-envelope"></span> sdgaw102@gmail.com</p>
            </div>
            <div class="col-sm-7 slideanim">
              <div class="row">
                <div class="col-sm-6 form-group">
                  <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
                </div>
                <div class="col-sm-6 form-group">
                  <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                </div>
              </div>
              <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <button class="btn btn-default pull-right" type="submit">Send</button>
                </div>
              </div>
            </div>
          </div>
        </div>
</asp:Content>
