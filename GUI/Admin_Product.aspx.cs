﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Service;
using System.IO;

namespace GUI
{
    public partial class Admin_Product : System.Web.UI.Page
    {

        BLL_Product product = new BLL_Product();
        BLL_Brand brand = new BLL_Brand();
        BLL_Catogory catogory = new BLL_Catogory();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BinData();


            }

        }
        bool CheckFileType(string fileName)
        {

            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".png":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                default:
                    return false;
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && FileUpload1.HasFile && CheckFileType(FileUpload1.FileName))
            {
                string fileName = "uploads/" + DateTime.Now.ToString("ddMMyyyy_hhmmss_tt_") + FileUpload1.FileName;
                string filePath = MapPath(fileName);
                FileUpload1.SaveAs(filePath);
                Image2.ImageUrl = fileName;
            }
        }

        protected void btnAddProduct_Click(object sender, EventArgs e)
        {
            Session["Sate"] = "Add";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Nhập thông tin sản phẩm ở dưới rồi ấn Save')", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtNameProduct.Text == "" && txtInfoProduct.Value.ToString() == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Chưa điền đủ thông tin')", true);
            }
            else
            {
                if (Page.IsValid && FileUpload1.HasFile && CheckFileType(FileUpload1.FileName))
                {
                    switch ((string)Session["Sate"])
                    {
                        case "Add":
                            AddProduct();
                            BinData();
                            break;
                        case "Update":
                            UppdateProduct();
                            BinData();
                            break;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Chưa chọn ảnh')", true);
                }
            }


        }
        protected void AddProduct()
        {
            string fileName = "uploads/" + DateTime.Now.ToString("ddMMyyyy_hhmmss_tt_") + FileUpload1.FileName;
            string filePath = MapPath(fileName);

            Image2.ImageUrl = fileName;

            string name = txtNameProduct.Text;
            string info = txtInfoProduct.Value.ToString();
            int category = Convert.ToInt32(cboCategory.SelectedValue.ToString());
            int brand = Convert.ToInt32(cboBrand.SelectedValue.ToString());
            string avt = "uploads/" + DateTime.Now.ToString("ddMMyyyy_hhmmss_tt_") + FileUpload1.FileName;
            int userId = Convert.ToInt32(Session["UserId"]);
            bool sessionAuth = (bool)Session["sessionAuth"];

            switch (product.addProduct(name, info, category, brand, avt, userId, sessionAuth))
            {
                case 200:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Thêm thành công')", true);
                    FileUpload1.SaveAs(filePath);
                    break;
                case 400:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Thêm thất bại)", true);
                    break;
                case 403:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn không đủ quyền)", true);
                    break;
            }



        }
        protected void BinData()
        {
            rptProduct.DataSource = product.getAllProducts();
            rptProduct.DataBind();

            cboBrand.DataSource = brand.getBrand();
            cboBrand.DataTextField = "tenth";
            cboBrand.DataValueField = "math";
            cboBrand.DataBind();

            cboCategory.DataSource = catogory.getCatogory();
            cboCategory.DataTextField = "tendm";
            cboCategory.DataValueField = "madm";
            cboCategory.DataBind();
        }
        protected void btnSua_Command(object sender, CommandEventArgs e)
        {
            int masp = Convert.ToInt32(e.CommandArgument.ToString());
            Session["masp"] = masp;
            Session["Sate"] = "Update";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Nhập thông tin sản phẩm ở dưới rồi ấn Save')", true);
        }
        protected void UppdateProduct()
        {


            string fileName = "uploads/" + DateTime.Now.ToString("ddMMyyyy_hhmmss_tt_") + FileUpload1.FileName;
            string filePath = MapPath(fileName);

            Image2.ImageUrl = fileName;

            int masp = Convert.ToInt32(Session["masp"]);
            string name = txtNameProduct.Text;
            string info = txtInfoProduct.Value.ToString();
            int category = Convert.ToInt32(cboCategory.SelectedValue.ToString());
            int brand = Convert.ToInt32(cboBrand.SelectedValue.ToString());
            string avt = "uploads/" + DateTime.Now.ToString("ddMMyyyy_hhmmss_tt_") + FileUpload1.FileName;
            int userId = Convert.ToInt32(Session["UserId"]);
            bool sessionAuth = (bool)Session["sessionAuth"];

            switch (product.editProduct(masp, name, info, category, brand, avt, userId, sessionAuth))
            {
                case 200:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Sửa thành công')", true);
                    FileUpload1.SaveAs(filePath);
                    break;
                case 400:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Sửa thất bại)", true);
                    break;
                case 403:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn không đủ quyền)", true);
                    break;
            }
        }

        protected void btnXoa_Command(object sender, CommandEventArgs e)
        {
            int masp = Convert.ToInt32(e.CommandArgument.ToString());
            Session["masp"] = masp;
            Session["Sate"] = "Update";
        }
    }
}
