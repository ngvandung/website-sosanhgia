﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Service;

namespace GUI
{
    public partial class home : System.Web.UI.Page
    {
        BLL_Product product = new BLL_Product();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind();
            }
        }
        protected void Bind()
        {
            
            Repeater1.DataSource = product.getTop4Product("Tivi");
            Repeater1.DataBind();
            Repeater2.DataSource = product.getTop4Product("Điện thoại");
            Repeater2.DataBind();
            Repeater3.DataSource = product.getTop4Product("Máy tính");
            Repeater3.DataBind();
            Repeater4.DataSource = product.getTop4Product("Tủ lạnh");
            Repeater4.DataBind();
        }
    }
}