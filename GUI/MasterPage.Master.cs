﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Service;
using System.Data;


namespace GUI
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        BLL_CatogoryType catogoryType = new BLL_CatogoryType();
        BLL_Catogory catogory = new BLL_Catogory();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind();
            }
        }
        protected void Bind()
        {
            Repeater1.DataSource = catogoryType.getCatogoryType();
            Repeater1.DataBind();

    
        }
        protected DataTable getCategory(int matl)
        {
            DataTable tb = catogory.getCatogoryByCatogoryTypeId(matl);
            return tb;

        }

    }
}