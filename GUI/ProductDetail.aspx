﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="GUI.ProductDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/public/css/ProductDetail.css" rel="stylesheet" />
    <link href="assets/public/css/home.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="PrductDetail">
        <div class="row">
            <div class="container">
                <div class="product-slide-container">
                    <div class="big-image">
                        <img src="https://img.sosanhgia.com/images/500x0/513d53939a974bb7869daf14818f4013/samsung-galaxy-j4-plus.jpeg" alt="Alternate Text" />

                    </div>
                </div>
                <div class="product-info-container">
                    <a href="https://www.sosanhgia.com/p132968-samsung-galaxy-j4-plus.html" title="Samsung Galaxy J4 Plus" onclick="return false">
                        <h1>Samsung Galaxy J4 Plus</h1>
                    </a>
                    <div class="product-short-desc">
                        <ul>
                            <li>Màn hình :6.0 inchs, 720 × 1480 Pixels</li>
                            <li>Màn hình :6.0 inchs, 720 × 1480 Pixels</li>                        
                            <li>Màn hình :6.0 inchs, 720 × 1480 Pixels</li>
                            <li>Màn hình :6.0 inchs, 720 × 1480 Pixels</li>
                            <li>Màn hình :6.0 inchs, 720 × 1480 Pixels</li>
                            <li>Màn hình :6.0 inchs, 720 × 1480 Pixels</li>
                        </ul>
                    </div>
                    <div class="priority-store">
                        <span>Giá tốt từ nơi bán:</span>
                        <span class="store-price product-price">2.970.000</span>
                        <div class="merchant-logo-wrapper" style="width:60px;">
                            <div class="merchant-logo" style="width:60px;">
                                <img style="width:60px;" src="https://img.sosanhgia.com/images/1ef1aee0e2cb435893d32fe07031ec0c/shopee-mall.jpg" title="Shopee">
                            </div>
                        </div>

                        

                        <a style="margin-left:10px;" href="/r/redirect.php?pm_id=134842724&amp;cat_id=6&amp;asub=direct~SSG" class="ssg-btn price-btn link-btn-1 red-text" title="Đến nơi bán Samsung Galaxy J4 Plus giá rẻ nhất" target="_blank" rel="nofollow" ssg-track="true" ssg-category="product" ssg-action="Top priority" ssg-label="Click Shopee" ssg-event="click">Đến nơi bán</a>
                    </div>
                    <div class="stores-summrary">
                        
                        Có 52 sản phẩm từ 24 nơi bán, giá từ
                        <span class="product-price"></span>
                        <span class="product-price"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="stores">
            <div class="container">
                <div class="row">
                    <div class="stores-title">
                        <h4>
                            Bảng giá bán
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="product-merchants">
                        <li class="list-product ">
                            <div class="img-product">
                                <img src="https://img.sosanhgia.com/images/80x0/72d9dc0f9dd2406980876ca27745085c/dien-thoai-samsung-galaxy-j4-+-moi-hang-chinh-hang-bao-hanh-12-thang.jpeg" alt="Alternate Text" />
                            </div>
                            <div class="name-product">
                               Điện Thoại Samsung Galaxy J4 + RAM 3GB/ROM 32GB Mới - Hàng chính hãng - Bảo hành 12 tháng - Shopee
                            </div>
                            <div class="img-thuonghieu">
                                <img src="https://img.sosanhgia.com/images/80x0/72d9dc0f9dd2406980876ca27745085c/dien-thoai-samsung-galaxy-j4-+-moi-hang-chinh-hang-bao-hanh-12-thang.jpeg" alt="Alternate Text" />
                            </div>
                             
                            
                            <div class="btndennoiban">
                                 <a href="#" class="btn btn-link">Click xem</a>
                             </div>
                        </li>
                         <li class="list-product ">
                                <div class="img-product">
                                    <img src="https://img.sosanhgia.com/images/80x0/72d9dc0f9dd2406980876ca27745085c/dien-thoai-samsung-galaxy-j4-+-moi-hang-chinh-hang-bao-hanh-12-thang.jpeg" alt="Alternate Text" />
                                </div>
                                <div class="name-product">
                                   Điện Thoại Samsung Galaxy J4 + RAM 3GB/ROM 32GB Mới - Hàng chính hãng - Bảo hành 12 tháng - Shopee
                                </div>
                                 <div class="img-thuonghieu">
                                    <img src="https://img.sosanhgia.com/images/80x0/72d9dc0f9dd2406980876ca27745085c/dien-thoai-samsung-galaxy-j4-+-moi-hang-chinh-hang-bao-hanh-12-thang.jpeg" alt="Alternate Text" />
                                </div>
                             
                                <div class="btndennoiban">
                                     <a href="#" class="btn btn-link">Click xem</a>
                                 </div>
                            </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
