﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Admin_Product.aspx.cs" Inherits="GUI.Admin_Product" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/public/css/Admin_Product.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
            <!-- Animated -->
        <div class="animated fadeIn">
            <div class="product-status mg-tb-15">
                <div class="container-fluid">
                    
                    <!--table_product-->
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="product-status-wrap">
                                <h4>Products List</h4>
                                <div class="add-product">
                                    <asp:LinkButton ID="btnAddProduct" OnClick="btnAddProduct_Click" runat="server">Add Product</asp:LinkButton>
                                    
                                </div>
                                <table>
                                    <tr>
                                        <th>Ảnh</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>                           
                                        <th>Thông tin</th>
                                        <th>madm</th>
                                        <th>math</th>
                                        <th>diemdanhgia</th>
                                        <th>matk</th>
                                    </tr>
                                    <asp:Repeater ID="rptProduct" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><img src="<%# Eval("anhdaidien") %>" alt="" /></td>
                                                <td><%# Eval("masp") %></td>
                                                <td><%# Eval("tensp") %></td>
                                                <td><%# Eval("thongtin") %></td>
                                                <td><%# Eval("madm") %></td>
                                                <td><%# Eval("math") %></td>
                                                <td><%# Eval("diemdanhgia") %></td>
                                                <td><%# Eval("matk") %></td>
                                                <td>
                                                    <asp:Button ID="btnSua" runat="server" Text="Sửa" CommandArgument='<%# Eval("masp") %>' OnCommand="btnSua_Command" CssClass="btn btn-warning" />
                                                    <asp:Button ID="btnXoa" runat="server" Text="Xóa" CommandArgument='<%# Eval("masp") %>' CssClass="btn btn-danger"/>
                                                
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                  
                                </table>
                                <div class="custom-pagination">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--form_product-->
                    <div class="row">
                        
                        <div class="col-lg-12">
                        <div class="product-detail" id="product-detai">
                            <div class="product-detail-header">
                                <h4>Chi tiết sản phẩm</h4>
                            </div>
                            <div class="card-body card-block">
                                
                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <div class="col col-md-12"><label for="text-input" class=" form-control-label">Tên sản phẩm</label></div>
                                            
                                            <div class="col-12 col-md-9">
                                                <asp:TextBox ID="txtNameProduct" CssClass="form-control" runat="server">Tên sản phẩm</asp:TextBox>

                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">                                       
                                        <div class="col-6 form-group">
                                            <div class="col col-md-3"><label for="select" class=" form-control-label">Nhãn hiệu </label></div>
                                            <div class="col-12 col-md-9">
                                                <asp:DropDownList ID="cboBrand" runat="server" CssClass="form-control">
                                                    <asp:ListItem>
                                                        Chọn nhãn hiệu
                                                    </asp:ListItem>
                                                </asp:DropDownList>
                       
                                            </div>
                                        </div>
                                        <div class="col-6 form-group">
                                            <div class="col col-md-3"><label for="select" class=" form-control-label">Danh mục</label></div>
                                            <div class="col-12 col-md-9">
                                                 <asp:DropDownList ID="cboCategory" runat="server" CssClass="form-control">
                                                    <asp:ListItem>
                                                        Chọn danh mục
                                                    </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="row">                                       
                                    <div class="col-12 form-group">
                                        <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Thông tin chi tiết</label></div>
                                        <div class="col-12 col-md-12">
                                            <textarea name="textarea-input" runat="server" id="txtInfoProduct" rows="9" placeholder="Content..." class="form-control"></textarea>
                                            
                                        </div>
                                    </div>
                                       
                                </div>
                                    <div class="row">                                       
                                        <div class="col-6 ">
                                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Ảnh đại diện</label></div>
                                            
                                            <div class="col">
                                                <asp:Image ID="Image2" runat="server" Width="30%" ImageUrl="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_5VPSveuYgjtLonlKrRlXauhVaW9fmgYzCj0JpgVwExt4AmjK"/>
                                               
                                                
                                            </div>
                                            <br />
                                            <div class="col col-md-9">
                                                <asp:FileUpload ID="FileUpload1" runat="server" Width="348px" Height="27px" CssClass="Upload"/>
                                                <br />
                                                <br />
                                                <asp:Button ID="btnUpload" runat="server" Text="Upload" Height="27px" OnClick="btnUpload_Click" CssClass="btn btn-primary btn-sm"/>
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="col-6 form-group">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="btn btn-primary btn-sm btnSave"/>
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-danger btn-sm" />
                                           
                                        </div>
                                    </div>
                                    
                                   
      
                        
                                    
                                
                            </div>
   
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>

            
        </div>

       
        <!-- .animated -->
    </div>
</asp:Content>
