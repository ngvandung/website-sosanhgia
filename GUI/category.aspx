﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="category.aspx.cs" Inherits="GUI.category" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/public/css/home.css" rel="stylesheet" />
    <link href="/assets/public/css/Danhmuc.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section">
    <div class="container">
        <h3 class="section-title">Thiết bị di động</h3>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-info danhmuc">
				
					<div class="panel-body">
                        <h4>Bộ lọc</h4>
                    <div class="card card-collapse">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0">
                                <a class="boloc" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    Giá
                                    <i class="fas fa-chevron-down"></i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" style="">
                            <div class="card-body">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="" checked=""> Tất Cả
                                                                
                                    </label>

                                </div>
                                                          
                            </div>
                        </div>
                    </div>
                    <div class="card card-collapse">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0">
                                <a class="boloc" data-toggle="collapse" href="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo">
                                    Danh mục
                                    <i class="fas fa-chevron-down"></i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo2" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" style="">
                            <div class="card-body">
                                <div class="form-check">
                                    <ul>
                                        <li>Điện thoại</li>
                                        <li>Máy tính</li>
                                    </ul>

                                </div>
                                                          
                            </div>
                        </div>
                    </div>
					</div>
				</div>
            </div>
            <div class="col-md-9">
                <ul class="list_product">
					<li >
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    <li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    <li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    <li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    <li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    <li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    <li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    					<li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
                    					<li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>					<li>
						<div class="img-wrap">
				            <a href=""><img src="https://img.websosanh.vn/v2/users/wss/images/android-tivi-4k-sony-43-inch/1qyp629voghua.jpg?compress=85&width=220"></a>
			            </div>
			            <div class="information" align="center">
				            <div class="title">
				                <h3><a href="">Android Tivi 4K Sony 43 inch KD-43X8000E - Đen</a></h3>
				            </div>
				            <div class="price">
				                <span>16.900.000 đ</span>
				                <br>
                                <asp:Button Text="So sánh giá" runat="server" CssClass="btn btn-danger"/>
				            </div>
			            </div>
					</li>
					
				</ul>
            </div>
        </div>
        <br>
    </div>
</div>
</asp:Content>
