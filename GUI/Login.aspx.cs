﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Service;

namespace GUI
{
    public partial class Login : System.Web.UI.Page
    {
        BLL_Account accounts = new BLL_Account();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnDangNhap_Click(object sender, EventArgs e)
        {

            if (accounts.Login(txtId.Value.ToString(), txtPass.Value.ToString()) == 200)
            {
                Session["DaDangNhap"] = true;
                Session["UserId"] = accounts.getUserId(txtId.Value.ToString(), txtPass.Value.ToString());
                Session["sessionAuth"] = true;
                
                Response.Redirect("Admin_Product.aspx");
            }
        }
    }
}