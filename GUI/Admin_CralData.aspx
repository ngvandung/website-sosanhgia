﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Admin_CralData.aspx.cs" Inherits="GUI.Admin_CralData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
            <!-- Animated -->
        <div class="animated fadeIn">
            <div class="product-status mg-tb-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-user">
                              <div class="card-header">
                                <h5 class="card-title">Thêm bài viết</h5>
                              </div>
                              <div class="card-body">
                                  <div class="row" style="height:500px;display:flex;justify-content:center;align-items:center">
                                        <asp:Button ID="btnCralData" runat="server" Text="Crawl Data" CssClass="btn btn-primary" Width="200px" Height="80px" OnClick="btnCralData_Click"/>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            
        </div>

       
        <!-- .animated -->
    </div>
</asp:Content>
