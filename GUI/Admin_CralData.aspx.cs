﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Controller;

namespace GUI
{
    public partial class Admin_CralData : System.Web.UI.Page
    {
        ControllerBusiness controller = new ControllerBusiness();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnCralData_Click(object sender, EventArgs e)
        {
            if (controller.crawlData() == 200)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Thành công')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Không thành công')", true);
            }


        }
    }
}