﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Voting
    {
        private int userId;
        private int productId;
        private int vote;

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public int Vote
        {
            get
            {
                return vote;
            }

            set
            {
                vote = value;
            }
        }
    }
}
