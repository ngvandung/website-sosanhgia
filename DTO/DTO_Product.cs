﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Product
    {
        private int productId;
        private string nameProduct;
        private string infoProduct;
        private int categoryId;
        private int brandId;
        private int point;
        private int userId;
        private string avt;

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public string NameProduct
        {
            get
            {
                return nameProduct;
            }

            set
            {
                nameProduct = value;
            }
        }

        public string InfoProduct
        {
            get
            {
                return infoProduct;
            }

            set
            {
                infoProduct = value;
            }
        }

        public int CategoryId
        {
            get
            {
                return categoryId;
            }

            set
            {
                categoryId = value;
            }
        }

        public int BrandId
        {
            get
            {
                return brandId;
            }

            set
            {
                brandId = value;
            }
        }

        public int Point
        {
            get
            {
                return point;
            }

            set
            {
                point = value;
            }
        }

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }

        public string Avt
        {
            get
            {
                return avt;
            }

            set
            {
                avt = value;
            }
        }
    }
}
