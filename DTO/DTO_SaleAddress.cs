﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_SaleAddress
    {
        private int saleAddressId;
        private int productId;
        private string link;
        private string saleAddressName;
        private string saleAddressImage;
        public int SaleAddressId
        {
            get
            {
                return saleAddressId;
            }

            set
            {
                saleAddressId = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public string Link
        {
            get
            {
                return link;
            }

            set
            {
                link = value;
            }
        }

        public string SaleAddressName
        {
            get
            {
                return saleAddressName;
            }

            set
            {
                saleAddressName = value;
            }
        }

        public string SaleAddressImage
        {
            get
            {
                return saleAddressImage;
            }

            set
            {
                saleAddressImage = value;
            }
        }
    }
}
