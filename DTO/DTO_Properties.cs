﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Properties
    {
        private int propertiesId;
        private string propertiesName;

        public int PropertiesId
        {
            get
            {
                return propertiesId;
            }

            set
            {
                propertiesId = value;
            }
        }

        public string PropertiesName
        {
            get
            {
                return propertiesName;
            }

            set
            {
                propertiesName = value;
            }
        }
    }
}
