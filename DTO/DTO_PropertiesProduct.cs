﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_PropertiesProduct
    {
        private int propertiesId;
        private int productId;
        private string value;

        public int PropertiesId
        {
            get
            {
                return propertiesId;
            }

            set
            {
                propertiesId = value;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }
    }
}
