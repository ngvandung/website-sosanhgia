﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Catogory
    {
        private int catogoryId;
        private string catogoryName;
        private int typeId;

        public int CatogoryId
        {
            get
            {
                return catogoryId;
            }

            set
            {
                catogoryId = value;
            }
        }

        public string CatogoryName
        {
            get
            {
                return catogoryName;
            }

            set
            {
                catogoryName = value;
            }
        }

        public int TypeId
        {
            get
            {
                return typeId;
            }

            set
            {
                typeId = value;
            }
        }
    }
}
