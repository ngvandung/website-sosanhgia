﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_PriceHistory
    {
        private int saleAddressId;
        private string date;
        private float price;
        private int productId;
        public int SaleAddressId
        {
            get
            {
                return saleAddressId;
            }

            set
            {
                saleAddressId = value;
            }
        }

        public string Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        public float Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }
    }
}
