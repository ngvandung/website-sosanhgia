﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Brand
    {
        private int brandId;
        private string brandName;

        public int BrandId
        {
            get
            {
                return brandId;
            }

            set
            {
                brandId = value;
            }
        }

        public string BrandName
        {
            get
            {
                return brandName;
            }

            set
            {
                brandName = value;
            }
        }
    }
}
