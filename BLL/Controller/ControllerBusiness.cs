﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BLL.Service;
namespace BLL.Controller
{
    public class ControllerBusiness
    {
        BLL_CatogoryType catogoryType = new BLL_CatogoryType();
        BLL_Catogory catogory = new BLL_Catogory();
        BLL_ProductImage productImage = new BLL_ProductImage();
        BLL_Product product = new BLL_Product();
        BLL_SaleAddress saleAddress = new BLL_SaleAddress();
        BLL_PriceHistory pricehistory = new BLL_PriceHistory();
        //websosanh.vn
        public int crawlData()
        {
            try
            {
                var linkweb = "https://websosanh.vn";

                var htmlDocument = new HtmlAgilityPack.HtmlDocument();
                var htmlDocumentDetail = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(crawl(linkweb).Result);

                var Products = htmlDocument.DocumentNode.Descendants("ul")
                    .Where(node => node.GetAttributeValue("class", "")
                        .Equals("dropdown-menu")).ToList();

                var ProductList = Products[0].Descendants("li").ToList();
                foreach (var _ProductList in ProductList)
                {
                    //Loai danh muc
                    string typeName = HtmlEntity.DeEntitize((_ProductList.Descendants("a").FirstOrDefault().InnerText));
                    if (catogoryType.checkCatogoryType(typeName) == false)
                        catogoryType.addCatogoryType(typeName);

                    var ProductLists = _ProductList.Descendants("li").ToList();
                    foreach (var productList in ProductLists)
                    {
                        if (string.Compare(HtmlEntity.DeEntitize(productList.Descendants("a").FirstOrDefault().InnerText), "Taxi Việt Nam") == 0)
                            break;
                        //danh muc
                        string catogoryName = HtmlEntity.DeEntitize((productList.Descendants("a").FirstOrDefault().InnerText));
                        int typeId = catogoryType.getCatogoryTypefromTypeName(typeName);
                        if (typeId != 0) catogory.addCatogory(catogoryName, typeId);

                        //Link toi danh muc
                        int catogoryId = catogory.getCatogoryId(catogoryName);
                        if (catogoryId != 0 && catogory.checkCatogory(catogoryName) == false)
                            get(productList.Descendants("a").FirstOrDefault().GetAttributeValue("href", "").ToString(), catogoryId, 1);
                    }
                }
                return 200;
            }
            catch(Exception e)
            {
                return 400;
            }
            
        }

        public Task<string> crawl(string str)
        {
            var httpClient = new HttpClient();

            var html = httpClient.GetStringAsync(str);

            return html;
        }

        public void get(string linkweb, int catogoryId, int brandId)
        {
            var htmlDocument = new HtmlAgilityPack.HtmlDocument();
            var htmlDocumentDetail = new HtmlAgilityPack.HtmlDocument();

            htmlDocument.LoadHtml(crawl(linkweb).Result);

            var Products = htmlDocument.DocumentNode.Descendants("ul")
                .Where(node => node.GetAttributeValue("class", "")
                    .Equals("list-item list-product-search")).ToList();

            var ProductLists = Products[0].Descendants("li")
                .Where(node => node.GetAttributeValue("class", "")
                .Equals("item ")).ToList();

            foreach (var ProductList in ProductLists)
            {
                string productName = "";
                //Ten san pham
                try
                {
                    productName = HtmlEntity.DeEntitize(ProductList.Descendants("h2").FirstOrDefault().Descendants("a").FirstOrDefault().InnerText);
                }
                catch
                {
                    productName = HtmlEntity.DeEntitize(ProductList.Descendants("h3").FirstOrDefault().Descendants("a").FirstOrDefault().InnerText);
                }
                //Gia san pham
                string[] listPrice = ProductList.Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "").Equals("price ")).FirstOrDefault().InnerText.Trim().Split(' ');
                float price = float.Parse(listPrice[2]);

                //Link anh dai dien san pham
                string avt = ProductList.Descendants("img")
                    .Where(node => node.GetAttributeValue("class", "")
                        .Equals("lazyload")).FirstOrDefault().GetAttributeValue("data-src", "").ToString();

                string temp = "";
                try
                {
                    //Neu co tu 2 san pham tro len thi moi vao link chi tiet
                    if (HtmlEntity.DeEntitize(ProductList.Descendants("span").Where(node => node.GetAttributeValue("class", "").Equals("provins-text")).FirstOrDefault().InnerText).IndexOf("Xem") != -1)
                    {
                        //Link chi tiet => Chi tiet san pham
                        string linkDetail = ProductList.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                        htmlDocumentDetail.LoadHtml(crawl(linkDetail).Result);

                        //Thong tin san pham
                        try
                        {
                            var infoProducts = htmlDocumentDetail.DocumentNode.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("brief-desc")).FirstOrDefault().Descendants("li").ToList();

                            foreach (var infoProduct in infoProducts)
                            {
                                temp = temp + infoProduct.InnerText + ";";
                            }
                        }
                        catch (Exception e)
                        {

                        }

                        //link cac anh chi tiet cua san pham
                        var _productImages = htmlDocumentDetail.DocumentNode.Descendants("div")
                            .Where(node => node.GetAttributeValue("class", "").Equals("thumbnail")).FirstOrDefault().Descendants("li").ToList();

                        int productId = product.getProductIdFromProductName(productName);
                        foreach (var _productImage in _productImages)
                        {
                            string imageLinkDetail = _productImage.Descendants("img").FirstOrDefault().GetAttributeValue("src", "").ToString();
                            if(productImage.checkProductImage(imageLinkDetail) == false)
                                productImage.addProductImage(productId, imageLinkDetail);
                        }
                        ////Ma san pham
                        //string tempLinkDetail = linkDetail.Substring(21);
                        //string[] tempLinkDetail2 = tempLinkDetail.Split('/');
                        //listView1.Items.Add(tempLinkDetail2[1]);

                        var ProductsDetail = htmlDocumentDetail.DocumentNode.Descendants("table")
                            .Where(node => node.GetAttributeValue("class", "").Equals("table")).ToList();

                        var ProductListsDetail = ProductsDetail[0].Descendants("tr")
                            .Where(node => node.GetAttributeValue("class", "").Equals("line-solid")).ToList();
                        foreach (var ProductListDetail in ProductListsDetail)
                        {
                            string saleAddressName = "";
                            //Ten diem ban
                            try
                            {
                                saleAddressName = HtmlEntity.DeEntitize(ProductListDetail.Descendants("h2")
                                    .FirstOrDefault().Descendants("a").FirstOrDefault().InnerText);
                            }
                            catch
                            {
                                saleAddressName = HtmlEntity.DeEntitize(ProductListDetail.Descendants("h3")
                                    .FirstOrDefault().Descendants("a").FirstOrDefault().InnerText);
                            }
                            //Link diem ban
                            string saleAddressLink = ProductListDetail.Descendants("td")
                                .Where(node => node.GetAttributeValue("class", "").Equals("col-btn-redirect"))
                                    .FirstOrDefault().Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                            //Link anh diem ban
                            string saleAddressImage = ProductListDetail.Descendants("img")
                                .Where(node => node.GetAttributeValue("itemprop", "").Equals("image")).FirstOrDefault().GetAttributeValue("src", "");
                            if (saleAddress.checkSaleAddress(saleAddressName) == false)
                                saleAddress.addSaleAddress(productId, saleAddressName, saleAddressLink, saleAddressImage);
                            
                            //Gia moi diem ban
                            string[] listtSaleAddressPrice = ProductListDetail.Descendants("span")
                                .Where(node => node.GetAttributeValue("class", "").Equals("price")).FirstOrDefault().InnerText.Trim().Split(' ');
                            string[] tempPrice1 = listtSaleAddressPrice[0].Split('.');
                            string tempPrice2 = tempPrice1[0] + tempPrice1[1];
                            float saleAddressPrice = float.Parse(tempPrice2);
                            DateTime now = DateTime.Now;
                            string date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            int saleAddressId = saleAddress.getSaleAddressIdFromSaleAddressName(saleAddressName);

                            pricehistory.addPriceHistory(saleAddressId, productId, date, saleAddressPrice);
                        }
                        if (product.checkProduct(productName) == false)
                            product.addProduct(productName, temp, catogoryId, brandId, avt, 1998);
                    } 
                }
                catch
                {
                    try
                    {
                        //Neu co tu 2 san pham tro len thi moi vao link chi tiet
                        if (HtmlEntity.DeEntitize(ProductList.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("quantity")).FirstOrDefault().Descendants("p").FirstOrDefault().InnerText).IndexOf("Xem") != -1)
                        {
                            //Link chi tiet => Chi tiet san pham
                            string linkDetail = ProductList.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                            htmlDocumentDetail.LoadHtml(crawl(linkDetail).Result);
                            //Thong tin san pham
                            try
                            {
                                var infoProducts = htmlDocumentDetail.DocumentNode.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("brief-desc")).FirstOrDefault().Descendants("li").ToList();

                                foreach (var infoProduct in infoProducts)
                                {
                                    temp = temp + infoProduct.InnerText + ";";
                                }
                            }
                            catch(Exception e)
                            {

                            }
                            

                            //link cac anh chi tiet cua san pham
                            var _productImages = htmlDocumentDetail.DocumentNode.Descendants("div")
                                .Where(node => node.GetAttributeValue("class", "").Equals("thumbnail")).FirstOrDefault().Descendants("li").ToList();

                            int productId = product.getProductIdFromProductName(productName);
                            foreach (var _productImage in _productImages)
                            {
                                string imageLinkDetail = _productImage.Descendants("img").FirstOrDefault().GetAttributeValue("src", "").ToString();
                                if (productImage.checkProductImage(imageLinkDetail) == false)
                                    productImage.addProductImage(productId, imageLinkDetail);
                            }
                            ////Ma san pham
                            //string tempLinkDetail = linkDetail.Substring(21);
                            //string[] tempLinkDetail2 = tempLinkDetail.Split('/');
                            //listView1.Items.Add(tempLinkDetail2[1]);

                            var ProductsDetail = htmlDocumentDetail.DocumentNode.Descendants("table")
                                .Where(node => node.GetAttributeValue("class", "").Equals("table")).ToList();

                            var ProductListsDetail = ProductsDetail[0].Descendants("tr")
                                .Where(node => node.GetAttributeValue("class", "").Equals("line-solid")).ToList();
                            foreach (var ProductListDetail in ProductListsDetail)
                            {
                                string saleAddressName = "";
                                //Ten diem ban
                                try
                                {
                                    saleAddressName = HtmlEntity.DeEntitize(ProductListDetail.Descendants("h2")
                                        .FirstOrDefault().Descendants("a").FirstOrDefault().InnerText);
                                }
                                catch
                                {
                                    saleAddressName = HtmlEntity.DeEntitize(ProductListDetail.Descendants("h3")
                                        .FirstOrDefault().Descendants("a").FirstOrDefault().InnerText);
                                }
                                //Link diem ban
                                string saleAddressLink = ProductListDetail.Descendants("td")
                                    .Where(node => node.GetAttributeValue("class", "").Equals("col-btn-redirect"))
                                        .FirstOrDefault().Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                                //Link anh diem ban
                                string saleAddressImage = ProductListDetail.Descendants("img")
                                    .Where(node => node.GetAttributeValue("itemprop", "").Equals("image")).FirstOrDefault().GetAttributeValue("src", "");
                                if (saleAddress.checkSaleAddress(saleAddressName) == false)
                                    saleAddress.addSaleAddress(productId, saleAddressName, saleAddressLink, saleAddressImage);

                                //Gia moi diem ban
                                string[] listtSaleAddressPrice = ProductListDetail.Descendants("span")
                                    .Where(node => node.GetAttributeValue("class", "").Equals("price")).FirstOrDefault().InnerText.Trim().Split(' ');
                                string[] tempPrice1 = listtSaleAddressPrice[0].Split('.');
                                string tempPrice2 = tempPrice1[0] + tempPrice1[1];
                                float saleAddressPrice = float.Parse(tempPrice2);
                                DateTime now = DateTime.Now;
                                string date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                int saleAddressId = saleAddress.getSaleAddressIdFromSaleAddressName(saleAddressName);

                                pricehistory.addPriceHistory(saleAddressId, productId, date, saleAddressPrice);
                            }
                            if (product.checkProduct(productName) == false)
                                product.addProduct(productName, temp, catogoryId, brandId, avt, 1998);
                        }
                    }
                    //1 so loi bat thuong se bo qua
                    catch(Exception e)
                    {
                        continue;
                    }
                }
            }
        }
    }
}
