﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using DTO;

namespace BLL.Service
{
    public class BLL_Catogory
    {
        private DAL_Catogory dal_catogory = new DAL_Catogory();

        public DataTable getCatogory()
        {
            try
            {
                return dal_catogory.getCatogory();
            }
            catch
            {
                return null;
            }
        }

        public DataTable getCatogoryByCatogoryTypeId(int typeId)
        {
            try
            {
                return dal_catogory.getCatogoryByCatogoryTypeId(typeId);
            }
            catch
            {
                return null;
            }
        }

        public bool checkCatogory(string catogoryName)
        {
            return dal_catogory.checkCatogory(catogoryName).Rows.Count > 0 ? true : false;
        }

        public int getCatogoryId(string catogoryName)
        {
            try
            {
                return dal_catogory.getCatogoryId(catogoryName);
            }
            catch
            {
                return 0;
            }
        }

        public int addCatogory(string catogoryName, int typeId)
        {
            try
            {
                DTO_Catogory dto_catogory = new DTO_Catogory();

                dto_catogory.CatogoryId = dal_catogory.increment();
                dto_catogory.CatogoryName = catogoryName;
                dto_catogory.TypeId = typeId;

                return dal_catogory.addCatogory(dto_catogory) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteCatogory(int catogoryId)
        {
            try
            {
                return dal_catogory.deleteCatogory(catogoryId) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int editCatogory(int catogoryId, string catogoryName, int typeId)
        {
            try
            {
                DTO_Catogory dto_catogory = new DTO_Catogory();

                dto_catogory.CatogoryId = catogoryId;
                dto_catogory.CatogoryName = catogoryName;
                dto_catogory.TypeId = typeId;

                return dal_catogory.editCatogory(dto_catogory) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }
    }
}
