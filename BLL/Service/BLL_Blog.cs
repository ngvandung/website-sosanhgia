﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using BLL.Permission;

namespace BLL.Service
{
    public class BLL_Blog
    {
        private DAL_Blog dal_blog = new DAL_Blog();
        private CheckAuth checkPermission = new CheckAuth();
        public DataTable getBlog()
        {
            try
            {
                return dal_blog.getBlog();
            }
            catch
            {
                return null;
            }
        }

        public DataTable getBlogFromUserId(int userId)
        {
            try
            {
                return dal_blog.getBlogFromUserId(userId);
            }
            catch
            {
                return null;
            }
        }

        public int addBlog(string content, string image, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DateTime now = DateTime.Now;
                    DTO_Blog dto_blog = new DTO_Blog();

                    dto_blog.BlogId = dal_blog.increment();
                    dto_blog.Content = content;
                    dto_blog.Date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    dto_blog.UserId = userId;
                    dto_blog.Image = image;

                    if (dal_blog.addBlog(dto_blog))
                        return 200;
                    else return 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int addBlog(string content, string image, int userId)
        {
            try
            {
                DateTime now = DateTime.Now;
                DTO_Blog dto_blog = new DTO_Blog();

                dto_blog.BlogId = dal_blog.increment();
                dto_blog.Content = content;
                dto_blog.Date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                dto_blog.UserId = userId;
                dto_blog.Image = image;

                if (dal_blog.addBlog(dto_blog))
                    return 200;
                else return 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteBlog(int blogId, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    //if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                    //{
                        if (dal_blog.deleteBlog(blogId))
                            return 200;
                        else return 400;
                    //}
                    //else return 403;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int editProduct(int blogId, string content, string image, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DateTime now = DateTime.Now;
                    DTO_Blog dto_blog = new DTO_Blog();

                    dto_blog.BlogId = dal_blog.increment();
                    dto_blog.Content = content;
                    dto_blog.Date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    dto_blog.UserId = userId;
                    dto_blog.Image = image;

                    //if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                    //{
                        if (dal_blog.editBlog(dto_blog))
                            return 200;
                        else return 400;
                    //}
                    //else return 403;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }
    }
}
