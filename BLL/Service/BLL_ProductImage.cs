﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;

namespace BLL.Service
{
    public class BLL_ProductImage
    {
        private DAL_ProductImage dal_productimage = new DAL_ProductImage();

        public DataTable getProductImage()
        {
            try
            {
                return dal_productimage.getProductImage();
            }
            catch
            {
                return null;
            }
        }

        public bool checkProductImage(string linkImage)
        {
            return dal_productimage.checkProductImage(linkImage).Rows.Count > 0 ? true : false;
        }

        public DataTable getProductImageFromProductId(int productId)
        {
            try
            {
                return dal_productimage.getProductImageFromProductId(productId);
            }
            catch
            {
                return null;
            }
        }

        public int addProductImage(int productId, string image)
        {
            try
            {
                DTO_ProductImage dto_productimage = new DTO_ProductImage();

                dto_productimage.ProductId = productId;
                dto_productimage.ImageId = dal_productimage.increment(productId);
                dto_productimage.Image = image;

                return dal_productimage.addProductImage(dto_productimage) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteProductImage(int imageId)
        {
            try
            {
                return dal_productimage.deleteProductImage(imageId) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int editProductImage(int productId, int imageId, string image)
        {
            try
            {
                DTO_ProductImage dto_productimage = new DTO_ProductImage();

                dto_productimage.ProductId = productId;
                dto_productimage.ImageId = imageId;
                dto_productimage.Image = image;

                return dal_productimage.editProductImage(dto_productimage) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }
    }
}
