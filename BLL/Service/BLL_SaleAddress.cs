﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;
using BLL.Permission;

namespace BLL.Service
{
    public class BLL_SaleAddress
    {
        private DAL_SaleAddress dal_saledddress = new DAL_SaleAddress();
        private CheckAuth checkPermission = new CheckAuth();
        private CheckRole checkRole = new CheckRole();
        public DataTable getSaleAddress()
        {
            try
            {
                return dal_saledddress.getSaleAddress();
            }
            catch
            {
                return null;
            }
        }

        public bool checkSaleAddress(string catogoryName)
        {
            return dal_saledddress.checkSaleAddress(catogoryName).Rows.Count > 0 ? true : false;
        }

        public int getSaleAddressIdFromSaleAddressName(string saleAddressName)
        {
            return dal_saledddress.getSaleAddressIdFromSaleAddressName(saleAddressName);
        }

        public DataTable getSaleAddressFromProductId(int productId)
        {
            try
            {
                return dal_saledddress.getSaleAddressFromProductId(productId);
            }
            catch
            {
                return null;
            }
        }

        public int addSaleAddress(int productId, string saleAddressName, string link, int userId)
        {
            try
            {
                DTO_SaleAddress dto_salesddress = new DTO_SaleAddress();

                dto_salesddress.SaleAddressId = dal_saledddress.increment();
                dto_salesddress.ProductId = productId;
                dto_salesddress.Link = link;
                dto_salesddress.SaleAddressName = saleAddressName;
                if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                {
                    return dal_saledddress.addSaleAddress(dto_salesddress) == true ? 200 : 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int addSaleAddress(int productId, string saleAddressName, string link, string saleAddressImage)
        {
            try
            {
                DTO_SaleAddress dto_salesddress = new DTO_SaleAddress();

                dto_salesddress.SaleAddressId = dal_saledddress.increment();
                dto_salesddress.ProductId = productId;
                dto_salesddress.Link = link;
                dto_salesddress.SaleAddressName = saleAddressName;
                dto_salesddress.SaleAddressImage = saleAddressImage;

                    return dal_saledddress.addSaleAddress(dto_salesddress) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteSaleAddress(int dto_salesddress, int productId, int userId)
        {
            try
            {
                if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                {
                    return dal_saledddress.deleteSaleAddress(dto_salesddress) == true ? 200 : 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int editSaleAddress(int saleAddressId, int productId, string saleAddressName, string link, string saleAddressImage, int userId)
        {
            try
            {
                DTO_SaleAddress dto_salesddress = new DTO_SaleAddress();

                dto_salesddress.SaleAddressId = saleAddressId;
                dto_salesddress.ProductId = productId;
                dto_salesddress.Link = link;
                dto_salesddress.SaleAddressName = saleAddressName;
                dto_salesddress.SaleAddressImage = saleAddressImage;

                if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                {
                    return dal_saledddress.editSaleAddress(dto_salesddress) == true ? 200 : 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }
    }
}
