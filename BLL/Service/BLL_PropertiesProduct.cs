﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;
using BLL.Permission;

namespace BLL.Service
{
    public class BLL_PropertiesProductProduct
    {
        private DAL_PropertiesProduct dal_propertiesproduct = new DAL_PropertiesProduct();
        private CheckAuth checkPermission = new CheckAuth();
        private CheckRole checkRole = new CheckRole();
        public DataTable getPropertiesProduct()
        {
            try
            {
                return dal_propertiesproduct.getPropertiesProduct();
            }
            catch
            {
                return null;
            }
        }

        public DataTable getPropertiesProductfromProductId(int productId)
        {
            try
            {
                return dal_propertiesproduct.getPropertiesProductfromProductId(productId);
            }
            catch
            {
                return null;
            }
        }

        public int addPropertiesProduct(int propertiesProductId, int productId, string value)
        {
            try
            {
                DTO_PropertiesProduct dto_propertiesproduct = new DTO_PropertiesProduct();

                dto_propertiesproduct.PropertiesId = propertiesProductId;
                dto_propertiesproduct.ProductId = productId;
                dto_propertiesproduct.Value = value;

                return dal_propertiesproduct.addPropertiesProduct(dto_propertiesproduct) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deletePropertiesProduct(int propertiesProductId, int productId, int userId)
        {
            try
            {
                if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                {
                    return dal_propertiesproduct.deletePropertiesProduct(propertiesProductId, productId) == true ? 200 : 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int editPropertiesProduct(int propertiesProductId, int productId, string value, int userId)
        {
            try
            {
                DTO_PropertiesProduct dto_propertiesproduct = new DTO_PropertiesProduct();

                dto_propertiesproduct.PropertiesId = propertiesProductId;
                dto_propertiesproduct.ProductId = productId;
                dto_propertiesproduct.Value = value;

                if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                {
                    return dal_propertiesproduct.editPropertiesProduct(dto_propertiesproduct) == true ? 200 : 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }
    }
}
