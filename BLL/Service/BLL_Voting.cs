﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using BLL.Permission;
using System.Data;

namespace BLL.Service
{
    public class BLL_Voting
    {
        private DAL_Voting dal_Votings;
        private CheckAuth checkPermission;

        public int addVoting(int userId, int productId, int vote, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DTO_Voting dto_voting = new DTO_Voting();

                    dto_voting.UserId = userId;
                    dto_voting.ProductId = productId;
                    dto_voting.Vote = vote;

                    if (dal_Votings.addVoting(dto_voting))
                        return 200;
                    else return 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteVoting(int productId, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    //if (checkRoleVoting.checkRole(VotingId, userId) || checkPermission.isAdmin(userId))
                    //{
                    if (dal_Votings.deleteVoting(userId, productId))
                        return 200;
                    else return 400;
                    //}
                    //else return false;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int editVoting(int userId, int productId, int vote, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DTO_Voting dto_Voting = new DTO_Voting();

                    dto_Voting.UserId = userId;
                    dto_Voting.ProductId = productId;
                    dto_Voting.Vote = vote;

                    //if (checkRoleVoting.checkRole(dto_Voting.VotingId, userId) || checkPermission.isAdmin(userId))
                    // {
                    if (dal_Votings.editVoting(dto_Voting))
                        return 200;
                    else return 400;
                    //}
                    // else return false;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }
    }
}
