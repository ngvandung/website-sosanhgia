﻿using BLL.Permission;
using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Service
{
    public class BLL_Product
    {
        private DAL_Product dal_products = new DAL_Product();
        private CheckAuth checkPermission = new CheckAuth();
        private CheckRole checkRole = new CheckRole();
        public DataTable getAllProducts()
        {
            try
            {
                return dal_products.getAllProducts();
            }
            catch
            {
                return null;
            }
        }

        public DataTable getTop4Product(int catogoryId)
        {
            try
            {
                return dal_products.getTop4Product(catogoryId);
            }
            catch
            {
                return null;
            }
        }

        public DataTable getProductsByProductId(int productId)
        {
            try
            {
                return dal_products.getProductsByProductId(productId);
            }
            catch
            {
                return null;
            }
        }

        public bool checkProduct(string productName)
        {
            return dal_products.checkProduct(productName).Rows.Count > 0 ? true : false;
        }

        public int getProductIdFromProductName(string productName)
        {
            try
            {
                return dal_products.getProductIdFromProductName(productName);
            }
            catch
            {
                return 0;
            }
        }

        public DataTable getByDanhMuc(int danhmuc)
        {
            try
            {
                return dal_products.getByDanhMuc(danhmuc);
            }
            catch
            {
                return null;
            }
        }

        public int addProduct(string nameProduct, string infoProduct, int categoryId, int brandId, string avt, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DTO_Product dto_product = new DTO_Product();

                    dto_product.ProductId = dal_products.increment();
                    dto_product.NameProduct = nameProduct;
                    dto_product.InfoProduct = infoProduct;
                    dto_product.CategoryId = categoryId;
                    dto_product.BrandId = brandId;
                    dto_product.Point = 0;
                    dto_product.UserId = userId;
                    dto_product.Avt = avt;

                    if (dal_products.addProduct(dto_product))
                        return 200;
                    else return 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int addProduct(string nameProduct, string infoProduct, int categoryId, int brandId, string avt, int userId)
        {
            try
            {
                    DTO_Product dto_product = new DTO_Product();

                    dto_product.ProductId = dal_products.increment();
                    dto_product.NameProduct = nameProduct;
                    dto_product.InfoProduct = infoProduct;
                    dto_product.CategoryId = categoryId;
                    dto_product.BrandId = brandId;
                    dto_product.Point = 0;
                    dto_product.UserId = userId;
                    dto_product.Avt = avt;

                    if (dal_products.addProduct(dto_product))
                        return 200;
                    else return 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteProduct(int productId, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                    {
                        if (dal_products.deleteProduct(productId))
                            return 200;
                        else return 400;
                    }
                    else return 403;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int editProduct(int productId, string nameProduct, string infoProduct, int categoryId, int brandId, string avt, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DTO_Product dto_product = new DTO_Product();

                    dto_product.ProductId = productId;
                    dto_product.NameProduct = nameProduct;
                    dto_product.InfoProduct = infoProduct;
                    dto_product.CategoryId = categoryId;
                    dto_product.BrandId = brandId;
                    dto_product.Point = dal_products.getPoint(productId);
                    dto_product.UserId = userId;
                    dto_product.Avt = avt;

                    if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                    {
                        if (dal_products.editProduct(dto_product))
                            return 200;
                        else return 400;
                    }
                    else return 403;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int getUserIdfromProductId(int productId)
        {
            return dal_products.getUserIdfromProductId(productId);
        }

        public int getPoint(int productId)
        {
            return dal_products.getPoint(productId);
        }
    }
}
