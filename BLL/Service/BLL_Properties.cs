﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL.Service
{
    public class BLL_Properties
    {
        private DAL_Properties dal_properties = new DAL_Properties();

        public DataTable getProperties()
        {
            try
            {
                return dal_properties.getProperties();
            }
            catch
            {
                return null;
            }
        }

        public int addProperties(string propertiesName)
        {
            try
            {
                DTO_Properties dto_properties = new DTO_Properties();

                dto_properties.PropertiesId = dal_properties.increment();
                dto_properties.PropertiesName = propertiesName;

                return dal_properties.addProperties(dto_properties) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteProperties(int propertiesId)
        {
            try
            {
                return dal_properties.deleteProperties(propertiesId) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int editProperties(int propertiesId, string propertiesName)
        {
            try
            {
                DTO_Properties dto_properties = new DTO_Properties();

                dto_properties.PropertiesId = propertiesId;
                dto_properties.PropertiesName = propertiesName;

                return dal_properties.editProperties(dto_properties) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }
    }
}
