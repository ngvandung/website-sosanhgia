﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;

namespace BLL.Service
{
    public class BLL_PriceHistory
    {
        private DAL_PriceHistory dal_pricehistory = new DAL_PriceHistory();

        public DataTable getPriceHistory()
        {
            try
            {
                return dal_pricehistory.getPriceHistory();
            }
            catch
            {
                return null;
            }
        }

        public bool checkPriceHistory(int productId, int saleAddressId)
        {
            return dal_pricehistory.checkPriceHistory(productId, saleAddressId).Rows.Count > 0 ? true : false;
        }

        public DataTable getPriceHistory_Product_SaleAdress(int saleAddressId, int productId)
        {
            try
            {
                return dal_pricehistory.getPriceHistory_Product_SaleAdress(saleAddressId, productId);
            }
            catch
            {
                return null;
            }
        }


        public int addPriceHistory(int saleAddressId, int productId, string date, float price)
        {
            try
            {
                DTO_PriceHistory dto_pricehistory = new DTO_PriceHistory();

                dto_pricehistory.SaleAddressId = saleAddressId;
                dto_pricehistory.ProductId = productId;
                dto_pricehistory.Date = date;
                dto_pricehistory.Price = price;

                return dal_pricehistory.addPriceHistory(dto_pricehistory) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deletePriceHistory(int saleAddressId, int productId, string date)
        {
            try
            {
                return dal_pricehistory.deletePriceHistory(saleAddressId, productId, date) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int editPriceHistory(int saleAddressId, int productId, string date, float price)
        {
            try
            {
                DTO_PriceHistory dto_pricehistory = new DTO_PriceHistory();

                dto_pricehistory.SaleAddressId = saleAddressId;
                dto_pricehistory.ProductId = productId;
                dto_pricehistory.Date = date;
                dto_pricehistory.Price = price;

                return dal_pricehistory.editPriceHistory(dto_pricehistory) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }
    }
}
