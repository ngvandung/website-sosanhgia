﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;

namespace BLL.Service
{
    public class BLL_CatogoryType
    {
        private DAL_CatogoryType dal_catogorytype = new DAL_CatogoryType();

        public DataTable getCatogoryType()
        {
            try
            {
                return dal_catogorytype.getCatogoryType();
            }
            catch
            {
                return null;
            }
        }


        public bool checkCatogoryType(string typeName)
        {
            return dal_catogorytype.checkCatogoryType(typeName).Rows.Count > 0 ? true : false;
        }

        public int getCatogoryTypefromTypeName(string typeName)
        {
            try
            {
                return dal_catogorytype.getCatogoryTypefromTypeName(typeName);
            }
            catch
            {
                return 0;
            }
        }

        public int addCatogoryType(string typeName)
        {
            try
            {
                DTO_CatogoryType dto_catogorytype = new DTO_CatogoryType();

                dto_catogorytype.TypeId = dal_catogorytype.increment();
                dto_catogorytype.TypeName = typeName;

                return dal_catogorytype.addCatogoryType(dto_catogorytype) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteCatogoryType(int typeId)
        {
            try
            {
                return dal_catogorytype.deleteCatogoryType(typeId) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public int editCatogoryType(int typeId, string typeName)
        {
            try
            {
                DTO_CatogoryType dto_catogorytype = new DTO_CatogoryType();

                dto_catogorytype.TypeId = typeId;
                dto_catogorytype.TypeName = typeName;

                return dal_catogorytype.editCatogoryType(dto_catogorytype) == true ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }
    }
}
