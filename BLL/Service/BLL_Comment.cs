﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using DTO;
using BLL.Permission;

namespace BLL.Service
{
    public class BLL_Comment
    {
        DAL_Comment dal_comment = new DAL_Comment();
        CheckAuth checkPermission = new CheckAuth();
        public DataTable getCommentFromProductId(int productId)
        {
            try
            {
                return dal_comment.getCommentFromProductId(productId);
            }
            catch
            {
                return null;
            }
        }

        public int addComment(int productId, string content, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DateTime now = DateTime.Now;
                    DTO_Comment dto_comment = new DTO_Comment();

                    dto_comment.CommentId = dal_comment.increment();
                    dto_comment.ProductId = productId;
                    dto_comment.Content = content;
                    dto_comment.UserId = userId;
                    dto_comment.Date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");


                    if (dal_comment.addComment(dto_comment))
                        return 200;
                    else return 400;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int deleteComment(int commentId, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    //if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                    //{
                        if (dal_comment.deleteComment(commentId))
                            return 200;
                        else return 400;
                    //}
                    //else return 403;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }

        public int editComment(int commentId, int productId, string content, int userId, bool sessionAuth)
        {
            try
            {
                if (checkPermission.checkAuth(sessionAuth))
                {
                    DateTime now = DateTime.Now;
                    DTO_Comment dto_comment = new DTO_Comment();

                    dto_comment.CommentId = commentId;
                    dto_comment.ProductId = productId;
                    dto_comment.Content = content;
                    dto_comment.UserId = userId;
                    dto_comment.Date = now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    //if (checkRole.checkRoleProduct(productId, userId) || checkPermission.isAdmin(userId))
                    //{
                        if (dal_comment.editComment(dto_comment))
                            return 200;
                        else return 400;
                   //}
                   //else return 403;
                }
                else return 403;
            }
            catch
            {
                return 400;
            }
        }
    }
}
